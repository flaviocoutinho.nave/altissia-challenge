# altissia-challenge

Altissia code challenge

This api exposes some functionality over FizzBuzz algorithm.

This application was developed using:

 - Java 11
 - Maven
 - Spring MVC

And tested using:

 - TestNG
 - JUnit 5
*** 
## Start application

Run ```docker-compose up``` to build and run application on ```localhost:8080``` 

***

##Rest API

Endpoints description below.

## FizzBuzz GET

### Request

`GET /fizzbuzz?number={number}`

    curl -i -H 'Accept: application/json' http://localhost:8080/fizzbuzz?number=3

### Response

#### Success
    200 OK
   
    Fizz

If no parameter is present, the default behaviour is to return a list with FizzBuzz applied from 1 to 100

`GET /fizzbuzz?number=`

    curl -i -H 'Accept: application/json' http://localhost:8080/fizzbuzz?number=

### Response

#### Success
    200 OK
   
    ["1", "2", "Fizz", "4", "Buzz", "6", ...]


#### Error
    400

    param was not integer
## FizzBuzz POST

### Request

`POST /fizzbuzz`

    curl -i -H 'Accept: application/json' -d '["1", "3", "5", "10", "21"]' http://localhost:8080/fizzbuzz

### Response

    200 OK

    ["1", "Fizz", "Buzz", "10", "FizzBazz"]


***
