package com.br.coutinho.altissiachallenge.controller;

import com.br.coutinho.altissiachallenge.controller.FizzBuzzController;
import com.br.coutinho.altissiachallenge.service.impl.FizzBuzzServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {FizzBuzzController.class, FizzBuzzServiceImpl.class})
@WebMvcTest
class FizzBuzzControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void fizzBuzzList_withSuccess() throws Exception {
        String param = "[\"1\", \"3\", \"5\", \"7\", \"15\", \"21\", \"35\", \"105\"]";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/fizzbuzz")
                        .content(param)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultDOW = result.getResponse().getContentAsString();
        String expected = "[\"1\",\"Fizz\",\"Buzz\",\"Bazz\",\"FizzBuzz\",\"FizzBazz\",\"BuzzBazz\",\"FizzBuzzBazz\"]";
        assertNotNull(resultDOW);
        assertEquals(expected, resultDOW);
    }

    @Test
    public void fizzBuzzEmptyList_withSuccess() throws Exception {
        String param = "";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/fizzbuzz")
                        .content(param)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultDOW = result.getResponse().getContentAsString();
        String expected = "[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"Bazz\",\"8\",\"Fizz\",\"Buzz\",\"11\",\"Fizz\",\"13\",\"Bazz\",\"FizzBuzz\",\"16\",\"17\",\"Fizz\",\"19\",\"Buzz\",\"FizzBazz\",\"22\",\"23\",\"Fizz\",\"Buzz\",\"26\",\"Fizz\",\"Bazz\",\"29\",\"FizzBuzz\",\"31\",\"32\",\"Fizz\",\"34\",\"BuzzBazz\",\"Fizz\",\"37\",\"38\",\"Fizz\",\"Buzz\",\"41\",\"FizzBazz\",\"43\",\"44\",\"FizzBuzz\",\"46\",\"47\",\"Fizz\",\"Bazz\",\"Buzz\",\"Fizz\",\"52\",\"53\",\"Fizz\",\"Buzz\",\"Bazz\",\"Fizz\",\"58\",\"59\",\"FizzBuzz\",\"61\",\"62\",\"FizzBazz\",\"64\",\"Buzz\",\"Fizz\",\"67\",\"68\",\"Fizz\",\"BuzzBazz\",\"71\",\"Fizz\",\"73\",\"74\",\"FizzBuzz\",\"76\",\"Bazz\",\"Fizz\",\"79\",\"Buzz\",\"Fizz\",\"82\",\"83\",\"FizzBazz\",\"Buzz\",\"86\",\"Fizz\",\"88\",\"89\",\"FizzBuzz\",\"Bazz\",\"92\",\"Fizz\",\"94\",\"Buzz\",\"Fizz\",\"97\",\"Bazz\",\"Fizz\",\"Buzz\"]";
        assertNotNull(resultDOW);
        assertEquals(expected, resultDOW);
    }

    @Test
    public void fizzBuzzSimple_withSuccess() throws Exception {
        String param = "3";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/fizzbuzz?number=" + param)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultDOW = result.getResponse().getContentAsString();
        String expected = "Fizz";
        assertNotNull(resultDOW);
        assertEquals(expected, resultDOW);
    }

    @Test
    public void fizzBuzzSimple_withError() throws Exception {
        String param = "3.2";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/fizzbuzz?number=" + param)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        String resultDOW = result.getResponse().getContentAsString();
        String expected = "param was not integer";
        assertNotNull(resultDOW);
        assertEquals(expected, resultDOW);
    }

}