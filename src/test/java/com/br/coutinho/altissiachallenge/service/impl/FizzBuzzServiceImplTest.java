package com.br.coutinho.altissiachallenge.service.impl;

import com.br.coutinho.altissiachallenge.service.FizzBuzzService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FizzBuzzServiceImplTest {

    private final FizzBuzzService victim = new FizzBuzzServiceImpl();

    @Test(dataProvider = "FizzBuzzData")
    public void calculateFizzBuzz_withSuccess(String expected, Integer input){
        String result = victim.fizzbuzz(input);

        assertEquals(result, expected);
    }

    @DataProvider(name = "FizzBuzzData")
    private Object[][] simpleFizzBuzzDataSamples(){
        return new Object[][]{
                {"1", 1},
                {"2", 2},
                {"Fizz", 3},
                {"4", 4},
                {"Buzz", 5},
                {"Fizz", 6},
                {"Bazz", 7},
                {"8", 8},
                {"Fizz", 9},
                {"Buzz", 10},
                {"11", 11},
                {"Fizz", 12},
                {"13", 13},
                {"Bazz", 14},
                {"FizzBuzz", 15},
                {"FizzBazz", 21},
                {"BuzzBazz", 35},
                {"FizzBuzzBazz", 105}
        };
    }

}