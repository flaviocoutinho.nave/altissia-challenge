package com.br.coutinho.altissiachallenge.service;

public interface FizzBuzzService {

    String fizzbuzz(Integer number);
}
