package com.br.coutinho.altissiachallenge.service.impl;

import com.br.coutinho.altissiachallenge.service.FizzBuzzService;
import org.springframework.stereotype.Service;

@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {

    @Override
    public String fizzbuzz(Integer number) {
        return calculateFizzBuzzBazz(number);
    }

    private String calculateFizzBuzz(Integer number){
        return (number % 15 == 0) ? "FizzBuzz" :
                (number %  3 == 0) ? "Fizz" :
                        (number %  5 == 0) ? "Buzz" :
                                number.toString();
    }

    private String calculateFizzBuzzBazz(Integer number){
        return (number % 105 == 0) ? "FizzBuzzBazz" :
                (number % 35 == 0) ? "BuzzBazz" :
                    (number % 21 == 0) ? "FizzBazz" :
                        (number % 7 == 0) ? "Bazz" :
                            calculateFizzBuzz(number);
    }
}
