package com.br.coutinho.altissiachallenge.controller;

import com.br.coutinho.altissiachallenge.service.FizzBuzzService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
public class FizzBuzzController {

    private final FizzBuzzService service;

    public FizzBuzzController(FizzBuzzService service) {
        this.service = service;
    }

    @GetMapping(value = "fizzbuzz")
    public ResponseEntity getSimpleFizzBuzz(@PathParam(value = "number") Integer number){
        var result = number != null ? service.fizzbuzz(number) : simpleFizzBuzzEmptyRequest();
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    }

    @PostMapping(value = "fizzbuzz")
    public ResponseEntity<List<String>> getListFizzBuzz(@RequestBody(required = false) List<Integer> numbers){
        var fizzbuzzResult = (numbers == null ||numbers.isEmpty()) ?
                simpleFizzBuzzEmptyRequest() :
                numbers
                    .stream()
                    .map(service::fizzbuzz)
                    .collect(Collectors.toList());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(fizzbuzzResult);
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity handleNotIntegerParamException(NumberFormatException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("param was not integer");
    }

    private List<String> simpleFizzBuzzEmptyRequest(){
        List<String> result = new ArrayList<>();
        for(int i = 1; i < 101; i++){
            result.add(service.fizzbuzz(i));
        }

        return result;
    }
}
