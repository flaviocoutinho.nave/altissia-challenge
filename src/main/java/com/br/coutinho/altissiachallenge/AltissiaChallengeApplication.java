package com.br.coutinho.altissiachallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AltissiaChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltissiaChallengeApplication.class, args);
	}

}
